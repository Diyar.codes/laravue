@extends('layout.main')

@section('content')
<!-- Content Row -->
<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Show Product</h6>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <strong>Name:</strong>
                    {{ $product->name }}
                </div>
                <div class="form-group">
                    <strong>Details:</strong>
                    {{ $product->detail }}
                </div>
                <div class="form-group">
                    <strong>Image:</strong>
                    <img src="/image/{{ $product->image }}" width="500px">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
