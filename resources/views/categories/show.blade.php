@extends('layout.main')

@section('content')
<!-- Content Row -->
<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Show Categories</h6>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <strong>Name:</strong>
                    {{ $category->name }}
                </div>
                <div class="form-group">
                    <strong>Details:</strong>
                    {{ $category->detail }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
