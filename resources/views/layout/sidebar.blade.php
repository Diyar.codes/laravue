<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Laravue</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href={{ route('dashboard.index') }}>
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    @if(auth()->user())
    <!-- Divider -->
    <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            List
        </div>
    @endif

    @if(auth()->user())
        <li class="nav-item">
            <a class="nav-link" href={{ route('products.index') }}>
                <i class="fas fa-fw fa-table"></i>
                <span>Product</span></a>
        </li>
    @endif

    @if(auth()->user())
        <li class="nav-item">
            <a class="nav-link" href={{ route('categories.index') }}>
                <i class="fas fa-fw fa-table"></i>
                <span>Categories</span></a>
        </li>
    @endif

    @if (auth()->user() && auth()->user()->role === 'admin')
        <li class="nav-item">
            <a class="nav-link" href=#>
                <i class="fas fa-fw fa-table"></i>
                <span>Product Admin</span></a>
        </li>
    @endif


    @if (auth()->user() &&  auth()->user()->role === 'user')
        <li class="nav-item">
            <a class="nav-link" href=#>
                <i class="fas fa-fw fa-table"></i>
                <span>Product Member</span></a>
        </li>
    @endif

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
