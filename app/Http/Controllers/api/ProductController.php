<?php

namespace App\Http\Controllers\api;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $page = $request->input('page') ? $request->input('page') : 1;
        $limit = $request->input('limit') ? $request->input('limit') : 10;

        $sort = $request->input('sort') ? $request->input('sort') : 'id:desc';
        $q = $request->input('search') ? $request->input('search') : '';

        $filter = [];
        if ($request->has('name') && $request->name !== null) {
            $filter['name'] = $request->name;
        }
        if ($request->has('detail') && $request->detail !== null) {
            $filter['detail'] = $request->detail;
        }

        $query = Product::select('*')->with('category');

        if (!empty($params_extra)) {
            if (isset($params_extra['name'])) {
                $query->where('name', $params_extra['name']);
            }

            if (isset($params_extra['detail'])) {
                $query->where('detail', $params_extra['detail']);
            }
        }

        $total = $query->count();

        if ($q != "") {
            $query->where(
                function ($query) use ($q) {
                    return $query->where('name', 'like', "%$q%");
                }
            );
        }

        if ($sort != "") {
            $sort = explode(':', $sort);
            $query->orderBy($sort[0], @$sort[1]);
        }
        $total_filtered = $query->count();
        $limit = ($limit == -1) ? 10 : $limit;
        if (!is_null($limit)) {
            $offset = ($page - 1) * $limit;
            $query->offset($offset)->limit($limit);
        }

        $data_result = $query->get();
        $pagination = [
            "limit" => (int) $limit,
            "total_page" => ceil($total_filtered / $limit),
            "total_rows" => $total,
            "current_page" => (int) $page,
        ];

        $result = array(
            'meta' => [
                'code' => 200,
                'status' => true,
                'message' => 'list product'
            ],
            'data' => [
                'list' => $data_result,
                'pagination' => $pagination
            ]
        );

        return $result;
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'category_id' => 'required',
        ]);

        $input = $request->all();

        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }

        $product = Product::create($input);

        $result = array(
            'meta' => [
                'code' => 200,
                'status' => true,
                'message' => 'Product created!'
            ],
            'data' => [
                'detail' => $product
            ]
        );

        return $result;
    }

    public function show($id)
    {
        $product = Product::find($id);
        if(!$product) {
            $result = [
                'meta'=>array(
                    'code' => 404,
                    'status' => false,
                    'message' => 'product not found'
                )
            ];

            return $result;
        }

        $result = array(
            'meta' => [
                'code' => 200,
                'status' => true,
                'message' => 'Product show!'
            ],
            'data' => [
                'detail' => $product
            ]
        );

        return $result;
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
            'category_id' => 'required',
        ]);

        $input = $request->all();

        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }else{
            unset($input['image']);
        }

        $product = Product::find($id);
        if(!$product) {
            $result = [
                'meta'=>array(
                    'code' => 404,
                    'status' => false,
                    'message' => 'product not found'
                )
            ];

            return $result;
        }

        $product->update($input);

        $result = array(
            'meta' => [
                'code' => 200,
                'status' => true,
                'message' => 'Product updated!'
            ],
            'data' => [
                'detail' => $product
            ]
        );

        return $result;
    }
    public function destroy($id)
    {
        $product = Product::find($id);
        if(!$product) {
            $result = [
                'meta'=>array(
                    'code' => 404,
                    'status' => false,
                    'message' => 'product not found'
                )
            ];

            return $result;
        }

        $product->delete();

        $result = array(
            'meta' => [
                'code' => 200,
                'status' => true,
                'message' => 'Product deleted!'
            ],
            'data' => [
                'detail' => $product
            ]
        );

        return $result;
    }
}
