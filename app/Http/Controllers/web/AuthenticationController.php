<?php

namespace App\Http\Controllers\web;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
    public function register() {
        return view('authentication.register');
    }

    public function doRegister(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'role' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if($user) {
            return redirect()->back()->with('error', 'Email sudah terdaftar');
        }

        $input = $request->all();
        $input['password'] = bcrypt($request->password);

        User::create($input);

        return redirect()->back()->with('success', 'Registrasi Berhasil');
    }

    public function login() {
        return view('authentication.login');
    }

    public function doLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();

        if(!$user) {
            return redirect()->back()->with('error', 'Email tersebut tidak ditemukan');
        }

        $credentials = $request->only('email', 'password');

        $attempt = Auth::attempt($credentials);

        if ($attempt) {
            return redirect('/dashboard')->with('success', 'Login Successful!');
        } else {
            return redirect()->back()->with('error', 'Email atau Password salah');
        }
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login')->with('success', 'Logout Successful!');;
    }
}
