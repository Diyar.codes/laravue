<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'App\Http\Controllers\api'], function(){
    Route::post('register', 'AuthenticationController@register');
    Route::post('login', 'AuthenticationController@login');

    Route::middleware('auth:sanctum')->group( function () {
        Route::get('user', function (Request $request) {
            return $request->user();
        });

        Route::get('refresh-token', 'AuthenticationController@refresh');

        Route::group(['middleware' => 'admin', 'prefix' => 'products'], function(){
            Route::get('/', 'ProductController@index')->name('product.view');
            Route::get('/{id}', 'ProductController@show')->name('product.show');
            Route::post('/', 'ProductController@store')->name('product.store');
            Route::post('/{id}', 'ProductController@update')->name('product.updated');
            Route::delete('/{id}', 'ProductController@destroy')->name('product.delete');
        });

        Route::group(['middleware' => 'admin', 'prefix' => 'categories'], function(){
            Route::get('/', 'CategoryController@index')->name('category.view');
            Route::get('/{id}', 'CategoryController@show')->name('category.show');
            Route::post('/', 'CategoryController@store')->name('category.store');
            Route::post('/{id}', 'CategoryController@update')->name('category.updated');
            Route::delete('/{id}', 'CategoryController@destroy')->name('category.delete');
        });
    });

});
