<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('authentication.login');
});

// Route::get('{any}', function () {
//     return view('app');
// })->where('any', '.*');

Route::group(['namespace' => 'App\Http\Controllers\web'], function(){
    Route::get('/register', 'AuthenticationController@register')->name('register');
    Route::post('/do-register', 'AuthenticationController@doRegister')->name('do-register');

    Route::get('/login', 'AuthenticationController@login')->name('login');
    Route::post('/do-login', 'AuthenticationController@doLogin')->name('do-login');

    Route::get('/dashboard-guest', 'DashboardController@guest')->name('dashboard.guest');

    Route::middleware('auth')->group( function () {
        Route::get('/logout', 'AuthenticationController@logout')->name('logout');

        Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

        Route::resource('products', 'ProductController');
        Route::resource('categories', 'CategoryController');
    });
});
